const { responseStatus } = require("../utilities");
const createError = require("http-errors");
const joiSchemas = require("./joiSchemas");
const { validate } = require("./commands");

const joi = require("joi");

module.exports = async function validateQuery(command, query) {
  //save request parameters
  let validation;
  switch (command) {
    case validate.user.REGISTER:
      validation = joiSchemas.user.register().validate({
        email: query.email,
        areaCode: query.areaCode,
        phone: query.phone,
        password: query.password,
        repeatPassword: query.repeatPassword,
        userName: query.userName,
        name: query.name,
        surname: query.surname,
        sex: query.sex,
        birth: query.birth,
        country: query.country,
      });
      break;
    case validate.user.USER_VALIDATION:
      
      validation = joiSchemas.user.validation().validate({
        email: query.email,
        token: query.token,
      });
      
      break;
    default:
      return responseStatus(
        false,
        "Query validation error: command not found",
        createError(400, "Command not found")
      );
      break;
  }

  if (validation.error != undefined) {
    return responseStatus(
      false,
      "Query validation error",
      createError(400, validation.error.details[0].message)
    );
  }

  return responseStatus();
};
