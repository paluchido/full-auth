const register = require("./register.js");
const validation = require("./validation.js");

module.exports = { register, validation };
