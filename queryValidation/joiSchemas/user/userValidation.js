const joi = require("joi"); //module used for request query parameters verification

function registerValidation() {
  return joi.object({
    token: joi.string().length(parseInt(process.env.VALIDATION_TOKEN_LENGTH)).required(),
    email: joi.string().email(),
  });
}

module.exports = registerValidation;
