const joi = require("joi"); //module used for request query parameters verification

function register() {
  var datetime = new Date();

  return joi
    .object({
      email: joi.string().email().required(),
      areaCode: joi.string().min(2),
      phone: joi.string().min(8),
      password: joi
        .string()
        .pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})"))
        .required(),
      repeatPassword: joi.ref("password"),
      userName: joi.string().min(2).required(),
      name: joi.string().min(2),
      surname: joi.string().min(2),
      sex: joi.string().min(4).max(6),
      birth: joi.date().greater("1-1-1920").less(datetime),
      country: joi.string().min(2),
    })
    .with("email", "password")
    .with("password", "repeatPassword");
}

module.exports = register;
