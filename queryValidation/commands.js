const user = {
  REGISTER: "REGISTER",
  USER_VALIDATION: "USER_VALIDATION",
};

module.exports = { validate: { user } };
