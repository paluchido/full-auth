const commands = require("./commands");
const validate = require("./queryValidator");

module.exports = { commands, validate };
