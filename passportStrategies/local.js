const User = require("../models").user;
const passport = require("passport");
const { bcryptFuncs, responseStatus } = require("../utilities");
const LocalStrategy = require("passport-local").Strategy;

//SET STRATEGY FIELDS (user + password)
const customFields = { usernameField: "email", passwordField: "password" };

//AET CALLBACK
const verifyCallback = (email, password, done) => {
  User.findOne({ email: email })
    .then(async (user) => {
      if (!user) return done(null, false);
      if (user.active === false) return done(null, false);

      const isValid = await bcryptFuncs.verifyHash(
        user.salt,
        password,
        user.password
      );
      isValid ? done(null, user) : done(null, false);
    })
    .catch((err) => {
      done(err);
    });
};

async function init() {
  const strategy = new LocalStrategy(customFields, verifyCallback);
  passport.use(strategy);
}

module.exports = init;
