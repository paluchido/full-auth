const passport = require("passport");
const mongoStore = require("connect-mongo");
const session = require("express-session");
const localInit = require("./local");
const user = require("../models").user;

function initPassportStrategies(app, mongoUrl) {
  //INIT STRATEGIES
  localInit();

  //ADD PASSPORT SERIALIZATION AND DESERIALIZATION
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (userId, done) {
    user.findById(userId, function (err, user) {
      done(err, user);
    });
  });

  //USE SESSION MIDDLEWARE
  app.use(
    session({
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: true,
      store: new mongoStore({
        mongoUrl: mongoUrl,
        collection: "sessions",
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365, //one year
      },
    })
  );
}

module.exports = initPassportStrategies;
