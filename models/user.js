const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, required: true, unique: true },
  area_code: { type: String },
  phone: { type: String },
  active: { type: Boolean, required: true },
  password: { type: String, required: true },
  salt: { type: String, required: true },
  log_points: [{ type: Schema.Types.ObjectId, ref: "device" }],
  create_date: { type: Date, required: true },
  user_name: { type: String, required: true },
  account_validation_token: { type: String, required: true },
  metadata: {
    name: { type: String },
    surname: { type: String },
    sex: { type: String },
    birth: { type: String },
    country: { type: String },
  },
  chain: [{ type: Schema.Types.ObjectId, ref: "chain" }],
}).index({ area_code: 1, phone: 1 }, { unique: true });

module.exports = mongoose.model("user", userSchema);
