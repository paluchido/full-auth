const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const deviceSchema = new Schema({
  email_recovery: {
    last_attempt_date: { type: Date },
    sent_attempts: { type: Number },
    token: { type: String },
    token_input_attempts: { type: Number },
  },
  phone_recovery: {
    last_attempt_date: { type: Date },
    sent_attempts: { type: Number },
    token: { type: String },
    token_input_attempts: { type: Number },
  },
  consecutive_bad_log_attempts: { type: Number },
  last_log_attempt: { type: Date },
  trust: { type: Boolean, required: true },
  address: {
    address: { type: String },
    longitude: { type: Number },
    latitude: { type: Number },
    country: { type: String },
    postal_code: { type: Number },
  },
  account_logs: { type: Number },
  create_date: { type: Date, required: true },
  metadata: {
    type: Schema.Types.Mixed,
  },
});

module.exports = mongoose.model("device", deviceSchema);
