const { v1: uuidv1 } = require("uuid");
const { v2: uuidv2 } = require("uuid");
const { v3: uuidv3 } = require("uuid");
const { v4: uuidv4 } = require("uuid");
const { v5: uuidv5 } = require("uuid");
const randomstring = require("randomstring");

function v1() {
  return uuidv1();
}

function v2() {
  return uuidv2();
}

function v3() {
  return uuidv3();
}

function v4() {
  return uuidv4();
}

function v5() {
  return uuidv5();
}

/*{
 *  lenght: number,
 *  type: alphanumeric - [0-9 a-z A-Z]
 *  alphabetic - [a-z A-Z]
 *  numeric - [0-9]
 *  hex - [0-9 a-f]
 *  binary - [01]
 *  octal - [0-7]
 *}
 */
async function randomString(length, type) {
  return await randomstring.generate({ length, type });
}

module.exports = { v1, v2, v3, v4, v5, randomString };
