module.exports = function status(
  success = true,
  log = "operation was made succesfully",
  data = null
) {
  return { success: success, log: log, data: data };
};
