const bcrypt = require("bcrypt");

async function generateSalt() {
  return await bcrypt.genSalt(10);
}

async function generateHash(salt, password) {
  return await bcrypt.hash(password, salt);
}

async function verifyHash(salt, password, hashedPassword) {
  const newHash = await generateHash(salt, password);
  return newHash === hashedPassword;
}

module.exports = { generateSalt, generateHash, verifyHash };
