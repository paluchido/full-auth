const useragent = require("express-useragent"); //to get user device metadata

module.exports = function userAgent(req) {
  const source = req.headers["user-agent"];
  const ua = useragent.parse(source);
  return ua;
};
