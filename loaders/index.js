const initPassportStrategies = require("../passportStrategies");

module.exports = async function init(expressApp, mongoUrl) {
  await initPassportStrategies(expressApp, mongoUrl);
};
