require("dotenv").config();
const passport = require("passport");
const loaders = require("./loaders");

const { authenticate, login, register, validate } = require("./functions");

//process.env.NODE_ENV = 'production' //hide stack trace

function initialize(app, mongoUrl) {
  //loads functional middlerwares
  loaders(app, mongoUrl);

  //ADD PASSPORT MIDDLEWARE (must be added here or router is  declared first)
  app.use(passport.initialize());
  app.use(passport.session());
}

module.exports = { initialize, authenticate, login, register, validate };
