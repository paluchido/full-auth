const queryValidator = require("../queryValidation");
const { user } = require("../controllers");
const { responseStatus } = require("../utilities");

async function validate(query) {
  //VALIDATE REGISTER QUERY
  const validated = queryValidator.validate(
    queryValidator.commands.validate.user.USER_VALIDATION,
    query
  );
  if (validated) {
    const accountValidation = await user.validate(query.email, query.token);
    return accountValidation;
  }
}
module.exports = validate;
