const createError = require("http-errors");

function isAuth(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    next(createError(403, "You need to be logged in to make this request"));
  }
}

module.exports = isAuth;
