const isAuth = require("./isAuth");
const login = require("./login");
const register = require("./register");
const validate = require("./validate");
const find = require("../controllers/user").find;

module.exports = {isAuth, login, register, validate, find };


