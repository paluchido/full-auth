const passport = require("passport");

function login(req, res, next) {
  passport.authenticate("local")(req, res, next);
}

module.exports = login;
