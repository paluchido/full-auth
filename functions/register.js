const { userAgent, responseStatus } = require("../utilities");
const queryValidator = require("../queryValidation");
const { user } = require("../controllers");

async function register(query) {
  const validateQuery = await queryValidator.validate(
    queryValidator.commands.validate.user.REGISTER,
    query
  ); //VALIDATE REGISTER QUERY
  if (validateQuery.success) {
    const registerQ = await user.register(query); //REGISTER NEW USER
    if (!registerQ.success) {
      //IF REGISTER ERROR THEN:
      return registerQ;
    } else {
      //SAVE NEW DEVICE
      const newUser = registerQ.data;
      //const newDeviceMetadata = userAgent(req);
      const newDeviceMetadata = {};
      const newDeviceAttempt = await user.device.register(
        newUser,
        true,
        newDeviceMetadata
      );

      if (!newDeviceAttempt.success) {
        //IF NEW DEVICE COULDN'T BE MADE
        return newDeviceAttempt;
      } else {
        return responseStatus(true, "User and device successfully registered", {
          newDevice: newDeviceAttempt.data._id,
          activationToken: registerQ.data.account_validation_token,
          message: registerQ.log,
        });
      }
    }
  }
}

module.exports = register;
