const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const userSchema = require("../../models").user;

const { responseStatus, bcryptFuncs } = require("../../utilities");

async function oneBy(reference, type) {
  let user = undefined;
  let searchObj = {};
  searchObj[type] = reference;
  try {
    user = await userSchema.findOne(searchObj);
    if (!user) {
      return responseStatus(
        false,
        `There is not user with referenced "${reference}" as "${type}"`,
        null
      );
    }
    return responseStatus(true, `User successfully found`, user);
  } catch (err) {
    return responseStatus(
      false,
      "There was an error creating the account. " + err.message,
      createError(500, err.message)
    );
  }
}

//to do
async function manyBy(reference, type) {
  let user = undefined;
  try {
    user = await userSchema.findOne({ reference: reference });
    if (!user) {
      return responseStatus(
        false,
        `There is not user with referenced "${reference}" as "${type}"`,
        null
      );
    }
    return responseStatus(true, `User successfully found`, user);
  } catch (err) {
    return responseStatus(
      false,
      "There was an error creating the account. " + err.message,
      createError(500, err.message)
    );
  }
}

module.exports = { oneBy, manyBy };
