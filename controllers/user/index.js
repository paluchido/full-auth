const register = require("./register"); //requires request body
const findBy = require("./find").one; //requires user, trust, and metadata
const device = require("./devices");
const validate = require("./validate"); //requires email and validation_token

module.exports = { register, findBy, device, validate };
