const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const deviceSchema = mongoose.model("device");
const find = require("../find");

const { responseStatus, bcryptFuncs } = require("../../../utilities");

module.exports = async function getAll(user) {
  let devices = [];

  try {
    let logPoints;
    if (user.log_points !== undefined) {
      logPoints = user.log_points;
    } else {
      const userReq = await find.findOneBy("_id", user);
      if (!userReq.success) return userReq.data;
      else logPoints = userReq.data.log_points;
    }

    user.log_points.map(
      await ((deviceId) => {
        let device = deviceSchema.findById(deviceId);
        if (device !== undefined) devices.push(device);
      })
    );

    return responseStatus(true, `Devices gotten successfully`, devices);
  } catch (err) {
    return responseStatus(
      false,
      "There was an error getting the devices. Error: " + err.message,
      createError(500, err.message)
    );
  }
};
