const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const deviceSchema = mongoose.model("device");
const { responseStatus, uuid } = require("../../../utilities");

module.exports = async function registerDevice(user, trust, metadata) {
  const date = new Date();

  try {
    const device = await deviceSchema.create({
      create_date: date,
      trust: trust,
      consecutive_bad_log_attempts: 0,
      account_logs: 0,
      metadata: metadata,
      email_recovery: { sent_attempts: 0, token_input_attempts: 0 },
      phone_recovery: { sent_attempts: 0, token_input_attempts: 0 },
    });

    user.log_points.push(device._id);
    await user.save();

    return responseStatus(
      true,
      "New device was registered successfully",
      device
    );
  } catch (err) {
    return responseStatus(
      false,
      "There was an error adding this device: " + err.message,
      createError(500, err.message)
    );
  }
};
