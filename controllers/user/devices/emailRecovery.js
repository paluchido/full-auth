const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const deviceSchema = mongoose.model("device");
const { responseStatus, uuid } = require("../../../utilities");

async function add(device) {
  try {
    const date = new Date();
    const token = await uuid.randomString(
      process.env.VALIDATION_TOKEN_LENGTH,
      "alphanumeric"
    );

    device.email_recovery = {
      last_attempt_date: date,
      sent_attempts: device.email_recovery.sent_attempts + 1,
      token: token,
      token_input_attempts: device.email_recovery.sent_attempts + 1,
    };
    await device.save();

    return responseStatus(true, "New Email was sent successfully", token);
  } catch (err) {
    return responseStatus(
      false,
      "There was an error sending the email: " + err.message,
      createError(500, err.message)
    );
  }
}

module.exports = { add };
