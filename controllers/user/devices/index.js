const register = require("./register");
const emailRecovery = require("./emailRecovery");
const getAll = require("./getUserDevices");

module.exports = { register, emailRecovery, getAll };
