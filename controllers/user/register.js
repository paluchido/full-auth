const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const userSchema = require("../../models").user;
const { responseStatus, uuid, bcryptFuncs } = require("../../utilities");

module.exports = async function register({
  email,
  areaCode,
  phone,
  password,
  userName,
  name,
  surname,
  sex,
  birth,
  country,
}) {
  const salt = await bcryptFuncs.generateSalt();
  const hash = await bcryptFuncs.generateHash(salt, password);
  const date = new Date();

  const token = await uuid.randomString(
    process.env.VALIDATION_TOKEN_LENGTH,
    "alphanumeric"
  );

  try {
    const user = await userSchema.create({
      email: email,
      area_code: areaCode,
      phone: phone,
      password: hash,
      salt: salt,
      create_date: date,
      user_name: userName,
      account_validation_token: token,
      metadata: {
        name: name,
        surname: surname,
        sex: sex,
        birth: birth,
        country: country,
      },
      active: 0,
    });
    return responseStatus(true, "User was registered successfully", user);
  } catch (err) {
    return responseStatus(
      false,
      "There was an error creating the account. " + err.message,
      createError(500, err.message)
    );
  }
};
