const mongoose = require("mongoose");
const createError = require("http-errors"); //error creator module
const findOneUserBy = require("./find").oneBy;
const getUserDevices = require("./devices/getUserDevices");
const userSchema = mongoose.model("user");
const deviceSchema = mongoose.model("device");

const { responseStatus, bcryptFuncs } = require("../../utilities");

async function validate(email, validationToken) {
  try {
    const userQuery = await findOneUserBy(email, "email");
    if (!userQuery.success) return userQuery;

    if (userQuery.data.active) return responseStatus(false, "User was already activated", {
      userId: userQuery.data._id,
    });

    if (userQuery.data.account_validation_token === validationToken) {
      userQuery.data.active = true;
      await userQuery.data.save();
      return responseStatus(true, "User activated successfully", {
        userId: userQuery.data._id,
      });
    }
    return responseStatus(false, "Validation key is invalid");
  } catch (err) {
    return responseStatus(
      false,
      "There was an error valudating the account. Error: " + err.message,
      createError(500, err.message)
    );
  }
};

module.exports = validate;