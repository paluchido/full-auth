const chai = require("chai");
const functions = require("../functions");
const expect = chai.expect;

const mongoose = require("mongoose");
async function testInit() {
  await mongoose.connect("mongodb://localhost:27017/indexar_business", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    autoIndex: true,
    useFindAndModify: false,
  });
}

describe("FULL-AUTH tasks", () => {
  describe(`User registration`, () => {
    it("Should create a new user", async () => {
      const email = "viejomartelli@gmail.com";
      const password = "Hnvpuhkh1";
      const repeatPassword = "Hnvpuhkh1";
      const userName = "paluchi";
      const name = "Gonzalo";
      const surname = "Alessandrelli";
      const areaCode = "+54";
      const phone = "01130282469";
      const sex = "male";
      const birth = "1999-08-18";
      const country = "argentina";
      await testInit();
      const userRegReq = await functions.register({
        email,
        password,
        repeatPassword,
        userName,
        name,
        surname,
        areaCode,
        phone,
        sex,
        birth,
        country,
      });
      expect(userRegReq).to.have.property("success");
      expect(userRegReq).to.have.property("log");
      expect(userRegReq).to.have.property("data");
      expect(userRegReq.success).to.be.equal(true);
      expect(userRegReq.data).to.have.property("newDevice");
      expect(userRegReq.data).to.have.property("activationToken");
      expect(userRegReq.data).to.have.property("message");
    });
  });
  describe(`User login`, () => {
    it("Should return 'User must be activated to login'", async () => {
      const email = "viejomartelli@gmail.com";
      const password = "Hnvpuhkh1";
      const userRegReq = await functions.login({ email, password });
      console.log("~ userRegReq", userRegReq);
      expect(userRegReq.success).to.be.equal(false);
      expect(userRegReq.data).to.have.property("activationToken");
    });
  });
});
